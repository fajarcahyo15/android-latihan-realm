package latihan.fajar.belajarrealm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import latihan.fajar.belajarrealm.realm_table.User;

public class FormActivity extends AppCompatActivity {

    // component
    EditText txt_id,txt_nama,txt_alamat;
    Button btn_aksi;

    // functional var
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        // inisialisasi component
        txt_id = (EditText) findViewById(R.id.txt_id);
        txt_nama = (EditText) findViewById(R.id.txt_nama);
        txt_alamat = (EditText) findViewById(R.id.txt_alamat);
        btn_aksi = (Button) findViewById(R.id.btn_aksi);

        // inisialisasi realm
        realm = Realm.getDefaultInstance();

        // mendapatkan data dari halaman sebelumnya
        Intent intent = getIntent();
        final HashMap<String, String> hashMap = (HashMap<String,String>) intent.getSerializableExtra("data");

        // cek apakah form ingin di set ke tambah/edit
        if(hashMap.get("aksi").equals("edit")){
            kondisiEdit(hashMap);
        }

        // cek apakah id sudah ada
        txt_id.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                RealmResults<User> row = realm.where(User.class)
                        .equalTo("id",txt_id.getText().toString())
                        .findAll();
                if(row.size() != 0)
                    btn_aksi.setEnabled(false);
                else
                    btn_aksi.setEnabled(true);
                return false;
            }
        });

        btn_aksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (hashMap.get("aksi")) {
                    case "tambah" :
                        aksiTambah();
                        break;
                    case "edit" :
                        aksiEdit(hashMap.get("id"));
                        break;
                    default:
                        break;
                }
            }
        });
    }

    // kondisi awal saat halaman di akses dengan mode edit
    private void kondisiEdit(HashMap<String, String> hashMap) {
        txt_id.setText(hashMap.get("id"));
        txt_nama.setText(hashMap.get("nama"));
        txt_alamat.setText(hashMap.get("alamat"));
        btn_aksi.setText("Edit");
        txt_id.setEnabled(false);
    }

    // aksi tambah data
    private void aksiTambah() {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgrealm) {
                User user = bgrealm.createObject(User.class, txt_id.getText().toString());
                user.setNama(txt_nama.getText().toString());
                user.setAlamat(txt_alamat.getText().toString());
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Toast.makeText(getApplicationContext(), "Berhasil menambah data", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(FormActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Toast.makeText(FormActivity.this, (CharSequence) error,Toast.LENGTH_LONG).show();
            }
        });
    }

    // aksi edit data
    private void aksiEdit(final String id) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                User user = bgRealm.where(User.class).equalTo("id", id).findFirst();
                user.setNama(txt_nama.getText().toString());
                user.setAlamat(txt_alamat.getText().toString());
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Toast.makeText(getApplicationContext(),"Data berhasil diedit",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(FormActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Toast.makeText(FormActivity.this, (CharSequence) error,Toast.LENGTH_LONG).show();
            }
        });
    }
}
