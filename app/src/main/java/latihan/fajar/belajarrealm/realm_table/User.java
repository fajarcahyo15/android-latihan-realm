package latihan.fajar.belajarrealm.realm_table;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by fajar on 10/23/17.
 */

public class User extends RealmObject {

    // table user
    // field :
    //      String id (Primary Key) => uniq
    //      String nama (Required) => tidak boleh null
    //      String alamat

    @PrimaryKey
    private String id;

    @Required
    private String nama;

    private String alamat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
