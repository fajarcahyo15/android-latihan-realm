package latihan.fajar.belajarrealm;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmResults;
import latihan.fajar.belajarrealm.adapter_rv.RVUserAdapter;
import latihan.fajar.belajarrealm.realm_table.User;

public class MainActivity extends AppCompatActivity {

    // component
    RecyclerView rv_user;
    FloatingActionButton btn_tambah;

    // functional var
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // inisialisasi component
        rv_user = (RecyclerView) findViewById(R.id.rv_user);
        btn_tambah = (FloatingActionButton) findViewById(R.id.btn_tambah);

        // inisialisasi RecyclerView
        rv_user.setLayoutManager(new LinearLayoutManager(this));

        // inisialisasi realm
        Realm.init(this);
        realm = Realm.getDefaultInstance();

        // mendapatkan seluruh data
        RealmResults<User> userList = realm.where(User.class).findAll();

        // tampilkan data yang didapat ke recyclerview
        RVUserAdapter adapter = new RVUserAdapter(this,userList);
        adapter.notifyDataSetChanged();
        rv_user.setAdapter(adapter);

        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("aksi","tambah");

                Intent intent = new Intent(MainActivity.this, FormActivity.class);
                intent.putExtra("data",hashMap);
                startActivity(intent);
            }
        });
    }
}
