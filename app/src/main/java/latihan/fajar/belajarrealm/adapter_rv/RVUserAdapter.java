package latihan.fajar.belajarrealm.adapter_rv;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import latihan.fajar.belajarrealm.FormActivity;
import latihan.fajar.belajarrealm.R;
import latihan.fajar.belajarrealm.realm_table.User;

/**
 * Created by fajar on 10/23/17.
 */

public class RVUserAdapter extends RecyclerView.Adapter<RVUserAdapter.ViewHolder> {

    private List<User> userData;
    Context context;
    Realm realm;

    public RVUserAdapter(Context context, List<User> userData){
        this.userData = userData;
        this.context = context;
        this.realm = Realm.getDefaultInstance();
        // Log.d("Hasil",userData.toString());
    }

    @Override
    public RVUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_user, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RVUserAdapter.ViewHolder holder, final int position) {

        holder.tv_nama.setText(userData.get(position).getNama());
        holder.tv_alamat.setText(userData.get(position).getAlamat());

        holder.lv_user.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                // memanggil fungsi menampilkan dialog
                showDialogEditHapus(context,position);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return userData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_nama, tv_alamat;
        LinearLayout lv_user;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_nama = itemView.findViewById(R.id.tv_nama);
            tv_alamat = itemView.findViewById(R.id.tv_alamat);
            lv_user = itemView.findViewById(R.id.lv_user);
        }
    }

    // fungsi untuk membuat dialog box
    private void showDialogEditHapus(Context context, final int position) {

        // inisialisasi data yang diperlukan untuk membuat dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final String[] aksi = {"Edit","Hapus"};
        final String id = userData.get(position).getId();

        builder.setTitle("Aksi")
                .setItems(aksi, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int index) {

                        switch (index){
                            case 0 : // aksi edit
                                Log.d("Aksi","Edit "+id);
                                aksiEdit(id);
                                break;
                            case 1 : // aksi hapus
                                aksiHapus(id);
                                break;
                            default:
                                break;
                        }
                    }
                })
                .create();
        builder.show();
    }

    // aksi hapus data dari realm
    private void aksiHapus(String id) {

        try {
            realm.beginTransaction();
            RealmResults<User> row = realm.where(User.class).equalTo("id", id).findAll();
            row.deleteAllFromRealm();
            realm.commitTransaction();
            notifyDataSetChanged();
            Toast.makeText(context, "Data dengan ID "+id+" berhasil dihapus",Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(context, (CharSequence) e,Toast.LENGTH_SHORT).show();
        }
    }

    // aksi edit data
    private void aksiEdit(String id) {

        HashMap<String, String> data = new HashMap<>();
        User user = realm.where(User.class).equalTo("id", id).findFirst();

        data.put("id",user.getId());
        data.put("nama",user.getNama());
        data.put("alamat",user.getAlamat());
        data.put("aksi","edit");

        Intent i = new Intent(context, FormActivity.class);
        i.putExtra("data",data);
        context.startActivity(i);
    }

}
